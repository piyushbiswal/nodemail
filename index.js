'use strict';

let nodemail=require('nodemailer');

let transport=nodemail.createTransport({
	host:'smtp.gmail.com',
	port:465,// for TLS:465 
	secure:true,
	auth:{
    user: 'Enter your mail id',
    pass: 'Enter your password'
  },
  tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
    }
});

// sender and reciever information along with subject and body 
let mailOptions={
  from: 'Enter your sender mail id',
  to: 'Enter your reciever mail id',
  subject: 'Sending Email using Node.js',
  text: 'That was easy!',
  //html:'for sending HTML content'
};

// for verification of connection to the SMTP server
transport.verify(function(err,success){
	if(err){
		console.log('err is ',err);
	}
    else{
       console.log('HOLA ',success);	
    }
})

//for Sending out the mail to the concerned group PDL or indivisual mail id
transport.sendMail(mailOptions,function(err,info){
	if(err){
		console.log(err);
	}
	else{
		console.log('mail send and response as ', info.response);
	}
});


